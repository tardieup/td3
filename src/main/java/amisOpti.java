int sommeDiviseurs(int nbre){
    int somme = 0;
    for(int i=1; i<nbre; i++){
        if (nbre%i==0) {
            somme = somme+i;}}
    return somme;

}

void testAmis(int plafond){
    long timer = System.currentTimeMillis();
    for (int i = 1; i<=plafond; i++){
        int sommeI = sommeDiviseurs(i);
        double plafond2 = i*1.4+50 ;
        for (int j = i+1; j<=plafond2 ; j++){
            if ((j==sommeI) && (i==sommeDiviseurs(j))){
                float temps = (System.currentTimeMillis()-timer)/1000;
                Ut.afficherSL("les nombres " + i + " et " + j + " sont amis, "+temps+"sec");
            }
        }
    }
    float temps = (System.currentTimeMillis()-timer)/1000;
    Ut.afficherSL("temps fin: "+temps+"sec");

}

void main() {
    testAmis(1000000);
}