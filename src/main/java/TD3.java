int max2(int a, int b) {
    //PR: a et b entier
    //Action: retourne le plus grand entre a et b
    if ( a > b) {
        return a;
    }
    else {
        return b;
    }
}

int max3(int a, int b, int c) {
    //PR: a b et c des entiers
    //Action: retourne le nombre max parmis 3 entiers
    return max2(max2(a,b),c);
}
void testMax() {
    //PR: que des nombres entiers
    //Action: renvoi le plus grand nbre parmis 2 puis parmis 3
    Ut.afficher("entre un nombre entier:");
    int a = Ut.saisirEntier();
    Ut.afficher("entre un autre nombre entier:");
    int b = Ut.saisirEntier();
    Ut.afficher("nombre le plus grand des deux nombre est : " + max2(a, b));

    Ut.afficherSL(" ");

    Ut.afficher("entre un nombre entier:");
    int c = Ut.saisirEntier();
    Ut.afficher("entre un autre nombre entier:");
    int d = Ut.saisirEntier();
    Ut.afficher("entre un autre nombre entier:");
    int e = Ut.saisirEntier();
    Ut.afficher("nombre le plus grand des 3 :" + max3(d, d, e));
}

void repeteCarac(int n,char car) {
    //PR: un entier puis un seul caractère quelconque
    //Action: print n fois un caractère
    for (int i = 0; i < n; i++){
        Ut.afficher(car);
    }
}


void pyramide(int h, char c) {
    //PR: un entier h qui défini la hauteur et un caractère
    //Action: print une pyramide de caractère
    for (int i = 1; i <= h; i++) {
            repeteCarac(h-i,' ');
            repeteCarac(2*i-1,c);
            Ut.sautLigne();
        }
}

void testPyramide() {
    //PR: nbre entier pour la hauteur et charactere à répéter
    //Action: interface pour créer une pyramide de caractère
    Ut.afficher("saisi le nombre de ligne :");
    int lignes = Ut.saisirEntier();
    Ut.afficher("saisi un charactère :");
    char charactere = Ut.saisirCaractere();
    pyramide(lignes,charactere);
}

void afficheNombresCroissants(int nb1, int nb2){
    //PR: 2 nombres entiers nb1 et nb2 avec nb1 plus petit que nb2
    //Action: renvoie les unités de nb1 dans un ordre croissant jusqu'a que nb1 atteigne nb2
    while (nb1 <= nb2){
        Ut.afficher(nb1%10);
        Ut.afficher(" ");
        nb1 = nb1 +1;
    }
}

void afficheNombresDecroissants(int nb1, int nb2){
    //PR:
    //Action:
    while (nb1 <= nb2){
        Ut.afficher(nb2%10);
        Ut.afficher(" ");
        nb2 = nb2 - 1;
    }
}

void pyramideElaboree(int h) {
    //PR:
    //Action:
    int ligne = 0;
    for (int i = 1; i <= h; i++) {
        repeteCarac(h-i,' ');
        repeteCarac(h-i,' ');
        afficheNombresCroissants(i,i+ligne);
        afficheNombresDecroissants(i,i+ligne-1);
        Ut.sautLigne();
        ligne = ligne + 1;
    }
}

void testPyramideElaboree() {
    //PR:
    //Action:
    Ut.afficher("saisir le nombre de ligne à faire :");
    int lignes = Ut.saisirEntier();
    pyramideElaboree(lignes);
}

int nbChiffres (int n) {
    //PR:
    //Action:
    int compteur = 1 ;
    while (n>=10) {
        compteur = compteur+1;
        n = (n - n%10)/10;
    }
    return compteur;
}

int nbreChiffresAuCarre (int n) {return nbChiffres(n*n);}

int sommeDiviseurs(int nbre){
    //PR:
    //Action:
    int somme = 0;
    for(int i=1; i<nbre; i++){
        if (nbre%i==0) {
            somme = somme+i;
        }
    }
    return somme;
    //garder en mémoire la somme du diviseur du nombre juste avant
}

boolean amis(int p, int q){
    //PR:
    //Action:
    if ((p==sommeDiviseurs(q)) && (q==sommeDiviseurs(p))) {
        return true;
    }
    return false;
}

void testAmis(int plafond){
    //PR:
    //Action:
    for (int i = 1; i<=plafond; i++){
        for (int j = i+1; j<=i*1.22+50 ; j++){
            if (amis(i,j)==true){
                Ut.afficherSL("les nombres "+i+" et "+j+" sont amis");
            }
        }
    }
}

int racineParfaite(int c){
    //PR:
    //Action:
    for (int i = 1; i<c; i++){
        if (i*i==c){
            return(i);
        }
    }
    return(-1);
}

boolean estCarreParfait(int c){
    //PR:
    //Action:
    if (racineParfaite(c)>0){
        return true;
    }
    return false;
}

double hypotenuse(int cote1, int cote2){
    //PR:
    //Action:
    return Math.sqrt((cote1*cote1)+(cote2*cote2));
}
boolean coteEntiers(int a, int b){
    //PR:
    //Action:
    if (estCarreParfait(a*a+b*b)==true){
        return true;
    }
    return false;
}

int aireTriangleRectangle(int cote1, int cote2){
    //PR:
    //Action:
    return ((cote1*cote2)/2);
}

double perimetreTriangleRectangle(int cote1, int cote2){
    //PR:
    //Action:
    return (hypotenuse(cote1,cote2)+cote1+cote2);
        }

void triplet(int perimetreMax){
    //PR:
    //Action:
    int j = 1;
    for (int i = 1; perimetreMax>perimetreTriangleRectangle(i,j); i++){
        for (j = i; perimetreMax>perimetreTriangleRectangle(i,j); j++){
            if (hypotenuse(i,j)%1==0){System.out.println("coté 1: "+i+" coté 2: "+j+" hypoténuse: "+hypotenuse(i,j)+" périmetre : "+perimetreTriangleRectangle(i,j));}
        }
        j = 1;
    }

}

boolean syracusiens(int nombre, int nbMaxOp){
    int origine = nombre;
    int nbOp = 0;
    while ((nombre>1) && (nbMaxOp>nbOp)) {
        nbOp++;
        if (nombre%2==0) {
            nombre = nombre/2;
        } else {
            nombre = nombre*3+1;
        }
    }
    if (nombre==1) {
        Ut.afficherSL("le nombre " + origine + " est syracusien");
        return true;
    }
    Ut.afficherSL("le nombre " + origine + " n'est toujours pas syracusien au bout de "+nbMaxOp+" opérations");
    return false;
}
void main() {

    /*
    testMax();
    testPyramide();
    afficheNombresCroissants(88,91);
    Ut.sautLigne();
    afficheNombresDecroissants(88,91);
    pyramideElaboree(10);
    Ut.afficher(nbChiffres(104));
    Ut.sautLigne();
    Ut.afficher(nbChiffresDuCarre(4));
    amis(220,284);

    racineParfaite(4225);
    testPyramideElaboree();
    System.out.println(estCarreParfait(36));
    System.out.println(estCarreParfait(40));
    System.out.println(coteEntiers(5,12));

    triplet(50);
    */
    testAmis(100000);
    //18000 = 9min 20
    //for (int i=1; i<=7; i++){syracusiens(i,10);}




}