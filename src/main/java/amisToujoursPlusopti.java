int sommeDiviseurs(int nbre) {
    int somme = 0;
    for (int i = 1; i < nbre; i++) {
        if (nbre % i == 0) {
            somme = somme + i;
        }
    }
    return somme;
}

int[] tabDivPlafond(int plafond){
    int taille = plafond+1;
    int[] tabDiv=new int [taille];
    for(int i=0; i<taille; i++){
        tabDiv[i]=sommeDiviseurs(i);
    }
    return tabDiv;
}

void testAmis(int plafond){
    long timer = System.currentTimeMillis();
    int [] tabDiv = tabDivPlafond(plafond);
    for (int i = 1; i<=plafond; i++){
        int sommeI = tabDiv[i];
        double plafond2 = i*1.4+50 ;
        for (int j = i+1; j<=plafond2 && j<=plafond ; j++){
            if ((j==sommeI) && (i==tabDiv[j])){
                float temps = (System.currentTimeMillis()-timer)/1000;
                Ut.afficherSL("les nombres " + i + " et " + j + " sont amis, "+temps+"sec");
            }
        }
    }
    float temps = (System.currentTimeMillis()-timer)/1000;
    Ut.afficherSL("temps fin: "+temps+"sec");
}

void main() {
    testAmis(1000000);
}